# latex-pre-commit

## Getting Started

### Build

```sh
cd latex-pre-commit
docker buildx build -t latex-pre-commit .
```

### Using

```sh
docker run --rm -v `pwd`:/data latex-pre-commit pre-commit run --all-files
```

## CI Examples

### GitLab CI/CD pipelines

```yaml
# .gitlab-ci.yml
default:
  image: registry.gitlab.com/hitchhikers/container-images/latex-pre-commit:latest

stages:
  - test

# Check format of latex files are according to pre-commit config
check-fmt:
  stage: test
  before_script:
    - python3 --version
    - pip3 --version
    - pre-commit --version
    - latexindent --version || true
  script:
    - pre-commit run --all-files
  only:
    refs:
      - merge_requests
```
