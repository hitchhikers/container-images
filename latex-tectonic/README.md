# tectonic image

##

The tectonic cache folder is mounted for reusing.  Optional step.

```sh
$ docker run -it --rm -v (pwd):/data -v (pwd)/.tectonic:/root/.cache/Tectonic --name tekkit tectonic-docker
```

Some makefile targets have the option of running `make docker` which use this image.
